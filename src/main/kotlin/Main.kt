import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.max

fun main(args: Array<String>) {
    val sc = Scanner(System.`in`)
    val n = sc.nextInt()

    for (k in 0 until n){
        val h = sc.nextInt()
        val w = sc.nextInt()
        val room = Array(h) {Array(w) {0} }
        val numbers = Array(w + h){0}
        for (i in 0 until  h){
            for (j in 0 until w){
                numbers[i + j]++
            }
        }
        val guests = ArrayList<ArrayList<Int>>()
        for (i in 0 until  w + h){
            guests.add(ArrayList<Int>())
        }
        var guest = w * h
        for (i in numbers.indices){
            for (j in 0 until numbers[i]){
                guests[i].add(guest)
                guest--
            }
        }
        //println(guests.toString())
        for (i in 0 until  h){
            for (j in 0 until w){
                //println("$i $j")
                val index = guests[i + j].size - 1
                room[i][j] = guests[i + j][index]
                guests[i + j].removeAt(index)
            }
        }
        for (i in 0 until  h){
            for (j in 0 until w){
                print(room[i][j].toString() + " ")
            }
            println()
        }
    }
}